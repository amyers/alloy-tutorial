module examples/tutorial/Queue

sig Queue { root: lone Node }
sig Node  { next: lone Node }

fact nextNotReflexive { no n : Node | n = n.next }
fact allNodesBelongToSomeQueue {
	all n : Node | one q: Queue | n in q.root.*next
}
fact nextNotCyclic { no n: Node | n in n.^next }

pred show() {}
run show for 2

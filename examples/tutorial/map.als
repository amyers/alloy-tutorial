module examples/tutorial/Map

abstract sig Object {}
sig Key, Value extends Object {}
sig Map { values: Key -> lone Value }

assert mappingIsUnique {
	all m: Map, k: Key, v, v': Value |
		k -> v in m.values and
 		k -> v' in m.values
 		implies v = v'
}

fact {
	all k: Key | some v: Value, m: Map | k -> v in m.values
  all v: Value | some k: Key, m: Map | k -> v in m.values
}

pred show() { #Map = 1 }
// run show for 2
check mappingIsUnique for 15
